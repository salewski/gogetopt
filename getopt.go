package gogetopt

// Copyright by Eric S. Raymond
// SPDX-License-Identifier: BSD-2-Clause

import (
	"fmt"
	"strings"
)

// An emulation of Python getopt, short arguments only.  Use the
// options value return by walking through the kets order of
// dictionary traversal is unspecified.  Note that the value of an
// option specified without : is "-". not nil.  Has to be disinct from
// "" because of Go's conventions about maps.

func getopt(argv []string, shortopts string, longopts []string) (map[string]string, []string, error) {
	hasValue := make(map[byte]bool)
	for i := range shortopts {
		c := shortopts[i]
		if c != ':' {
			hasValue[c] = (i < len(shortopts)-1) && (shortopts[i+1] == ':')
		}
	}
	var options = make(map[string]string)
	var optind int
	for optind = 0; optind < len(argv); optind++ {
		arg := argv[optind]
		if arg[0] != '-' {
			break
		}
		// Found a long option
		if arg[1] == '-' {
			for _, possibility := range longopts {
				hasVal := strings.HasSuffix(possibility, "=")
				if arg == possibility && !hasVal {
					options[arg] = "-"
					goto ok
				} else if hasVal && strings.HasPrefix(arg, possibility) {
					idx := strings.Index(arg, "=")
					options[arg[:idx]] = arg[idx+1:]
					goto ok
				}
			}
			return options, argv[optind:], fmt.Errorf("unknown long option %s", arg)
		ok:
			continue
		}
		// Found a short-option cluster
		for j := range arg {
			if j == 0 {
				continue
			}
			c := arg[j]
			if h, ok := hasValue[c]; !ok {
				optind++
				return options, argv[optind:], fmt.Errorf("unknown short option %c in %s", c, arg)
			} else if h {
				optind++
				if optind >= len(argv) || argv[optind][0] == '-' {
					return options, nil, fmt.Errorf("missing option value for %c", c)
				}
				options[fmt.Sprintf("-%c", c)] = argv[optind]
			} else {
				options[fmt.Sprintf("-%c", c)] = "-"
			}
		}
	}
	return options, argv[optind:], nil
}

// end
